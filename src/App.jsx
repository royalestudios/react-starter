import React, { Component } from 'react'

import './App.scss'

import Routes from './Routes'
import { ContextProvider } from './context'

import { LocaleProvider } from 'antd'
import es_ES from 'antd/lib/locale-provider/es_ES'

class App extends Component {
  render() {
    return (
      <LocaleProvider locale={es_ES} >
        <ContextProvider>
          <Routes />
        </ContextProvider>
      </LocaleProvider>
    )
  }
}

export default App