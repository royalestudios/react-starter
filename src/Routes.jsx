import React from 'react'
import { Switch, Route } from 'react-router-dom'

import LoginHandler from './app/login/LoginHandler'
import { RouteCan } from './helpers/routes'

// COMPONENTS

import HelloWorld from './app/example/HelloWorld'
import NotFound from './app/other/NotFound'

const Routes = () => {
  
  return (
    <Switch>
      
      {/* Public routes go here */}
      <Route exact path="/" component={HelloWorld} />

      {/* Protect every other route with login */}
      <LoginHandler>
        <Switch>

          <Route exact path="/no_scope" component={HelloWorld} />
          <RouteCan I="read" a="example" exact path="/protected" component={HelloWorld} />

          {/* OTHER EXAMPLE ROUTES */}
          {/* <RouteCan I="read" a="patient" exact path="/patients" component={PatientList} /> */}
          {/* <RouteCan I="create" a="consult" exact path="/patients/:patientId/consult" component={PatientConsultCreate} /> */}

          {/* If no path matches display the Not Found component */}
          <Route component={NotFound} />

        </Switch>
      </LoginHandler>

    </Switch>
  )
}

export default Routes