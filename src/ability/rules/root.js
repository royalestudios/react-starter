import { AbilityBuilder } from '@casl/ability'

const { rules, can } = AbilityBuilder.extract()

can('manage', 'all')

export default rules