const config = {
  development: '',
  production: '',
}

config.base = config[process.env.NODE_ENV]

config.getHeaders = () => ({
  'Content-Type': 'application/json',
  'Authorization': localStorage.getItem('accessToken')
})

export default config