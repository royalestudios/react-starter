import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Input, Button } from '../../../components'

import './index.scss'

class LoginPage extends Component {

  render() {

    const { onLogin } = this.props
  
    return (
      <div className="hm-login" >
        <form onSubmit={onLogin}>
          <Input 
            name="email"
            placeholder="Correo"
            required
            type="email"
          />
          <Input.Password
            name="password"
            placeholder="Contraseña"
            required
            type="password"
          />
          <Button type="primary" htmlType="submit" >
            Ingresar
          </Button>
        </form>
      </div>
    )
  }

}

LoginPage.propTypes = {
  onLogin: PropTypes.func,
}

LoginPage.defaultProps = {
  onLogin: (e) => {e.preventDefault()}
}

export default LoginPage