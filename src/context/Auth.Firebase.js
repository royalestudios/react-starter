
import firebase from 'firebase/app'
import '@firebase/auth'
import api from '../api'

export default class Auth {
  constructor (updateState, getState) {
    this.setState = updateState
    this.getState = getState
  }

  onAuthStateChanged = () => {
    firebase.auth().onAuthStateChanged(user => {
      // if user get info from api
      if (user) this.getUser()
      // else set an empty user in context's state
      else this.setState({user: {}})
    })
  }

  // get user info from api
  getUser = () => {
    api.user.get((user) => {
      // set the user to the context's state
      this.setState({user})
    })
  }

  // LOG IN /OUT METHODS

  onLogin = (source) => {
    // use Google login as default source
    let provider = new firebase.auth.GoogleAuthProvider()

    // use facebook login
    if (source === 'facebook') {
      provider = new firebase.auth.FacebookAuthProvider()
    }

    // sign in with popup
    firebase.auth().signInWithPopup(provider)
    .catch((error) => {
      console.error(`Error ${error.code}: ${error.message}`)
    })
  }

  onLogout = () => {
    firebase.auth().signOut()
    .then((result) => {
      // remove user
      this.setState({user: {}})
    })
    .catch((error) => {
      console.error(`Error ${error.code}: ${error.message}`)
    })
  }
}