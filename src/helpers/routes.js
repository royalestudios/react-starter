import React from 'react'
import { Route } from 'react-router-dom'

import { ability } from '../ability'

import NotFound from '../app/other/NotFound'

export const RouteCan = React.memo((routeProps) => {
  const { I:i, on, a, component, render, ...props } = routeProps

  if (!ability.can(i, on || a)) return <Route {...props} component={NotFound} />

  return <Route {...props} component={component} render={render} />
})